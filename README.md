# Rebel alliance Message  API

Program which decodes a distress message and returns the coordinates of the source

## Demo and documentation

- [Interfaz Swagger UI](https://rebel-aliance.onrender.com/docs/)
- [Online Service API](https://rebel-aliance.onrender.com/)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install.

```bash
npm install
```

## Environment Variables

Create a directory at the root of the project with the name "config" and in it create a file with the name "local" with the environment variables:

```bash
PORT=3000
REDIS_URL='*host redis instance*'
```

## Requirements

- Node v14
- Redis Instance

## License
[MIT](https://choosealicense.com/licenses/mit/)