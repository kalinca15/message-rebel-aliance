import { Request, Response as ResExpress } from 'express'
import makeHandler from '@infrastructure/libs/mainHandler'
import { MessageController } from '@root/core/modules/message/messsage.controller'
import { satelliteMessageSplitSchema, satellitesMessageSchema } from '../schemas'

export default class MessageHandler {
  public static async topsecret(req: Request, res: ResExpress): Promise<void> {
    const handler = await makeHandler('Top Secret', MessageController.process, satellitesMessageSchema)
    return handler(req, res)
  }

  public static async save(req: Request, res: ResExpress): Promise<void> {
    const handler = await makeHandler('Top Secret save', MessageController.save, satelliteMessageSplitSchema)
    return handler(req, res)
  }

  public static async deduce(req: Request, res: ResExpress): Promise<void> {
    const handler = await makeHandler('Top Secret deduce', MessageController.deduce)
    return handler(req, res)
  }
}
