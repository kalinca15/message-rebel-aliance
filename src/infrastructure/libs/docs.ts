import swaggerUi from 'swagger-ui-express'

import { Request, Response } from 'express'

export default async (req: Request, res: Response) => {
  return res.send(
    swaggerUi.generateHTML(await import('../../../docs/tsoa/swagger.json'))
  )
}
