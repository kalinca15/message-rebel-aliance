import { IResponseController } from '@root/common/response'
import { Request, Response as ResExpress } from 'express'
import { getDependences, IDependences } from '../libs/dependences'
import ValidateObject from '@common/joi'
import Joi from 'joi'

export default async (name: string, controller: (dependences: IDependences, body?: any, params?: any, query?: any)=> Promise<IResponseController>, validate?: Joi.ObjectSchema<any>) => {
  const dependences = getDependences(name)
  return async (req: Request, res: ResExpress) => {
    let response
    try {
      if (validate) await ValidateObject(validate, req.body)
      response = await controller(dependences, req.body, req.params, req.query)
    } catch (error) {
      response = dependences.responseService.error(error)
    }
    res.status(response.statusCode).json(response)
  }
}
