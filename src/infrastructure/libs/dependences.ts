import { CacheRedis } from '@root/common/cache'
import { ICacheRepository } from '@root/common/cache/interface'
import { LogService } from '@root/common/logger'
import { ILogRepository } from '@root/common/logger/interfaces'
import { ITrilaterationRepository, TrilaterationRepository } from '@root/common/trilateration'
import { v4 as uuidv4 } from 'uuid'
import env from '@environment'
import { IResponseRepository, ResponseHandler } from '@root/common/response'

export interface IDependences {
    loggerService: ILogRepository,
    trilaterationService: ITrilaterationRepository
    cacheService: ICacheRepository
    responseService: IResponseRepository
}

export const getDependences = (nameController: string) : IDependences => {
  const requestId = uuidv4()
  const loggerService = new LogService(requestId)
  const cacheInstance = CacheRedis.getInstance(env.REDIS_URL, env.REDIS_EXPIRED, loggerService)
  const Response = new ResponseHandler(loggerService, nameController)
  return {
    loggerService,
    trilaterationService: new TrilaterationRepository(),
    cacheService: cacheInstance,
    responseService: Response
  }
}
