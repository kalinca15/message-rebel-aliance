
import express from 'express'
import helmet from 'helmet'
import morgan from 'morgan'
import router from '@root/infrastructure/routes'
import unknownRoutesMiddleware from './middlewares/unknownRoutes'
import swaggerUi from 'swagger-ui-express'
import docs from './libs/docs'

const app = express()

app.use(express.json())
app.use(morgan('tiny'))
app.use(helmet.hidePoweredBy())

app.use('/', router)
app.use('/docs', swaggerUi.serve, docs)
app.use(unknownRoutesMiddleware)

export default app
