import { Request, Response } from 'express'

export default (req: Request, res: Response): void => {
  res.status(404).json({ code: 'NOT_FOUND', statusCode: 404, message: 'Service not found' })
}
