import { Router } from 'express'
import MessageHandler from '@root/infrastructure/handlers/message.handler'

const router = Router()

router.route('/status').get((req, res) => res.status(200).send('OK'))
router.route('/topsecret').post(MessageHandler.topsecret)
router.route('/topsecret/split/:satellite').post(MessageHandler.save)
router.route('/topsecret/split').get(MessageHandler.deduce)

export default router
