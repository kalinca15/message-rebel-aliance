import Joi from 'joi'

export const satellitesMessageSchema = Joi.object().keys({
  satellites: Joi.array().items(Joi.object().keys({
    name: Joi.string().valid('kenobi', 'skywalker', 'sato'),
    distance: Joi.number().required(),
    message: Joi.array()
  }))
})

export const satelliteMessageSplitSchema = Joi.object().keys({
  distance: Joi.number().required(),
  message: Joi.array()
})
