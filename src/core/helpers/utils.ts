export const isEquals = (oneData: any, secondData: any) => {
  return oneData === secondData
}

export const findObjInArray = <T>(
  list: T[],
  key: string,
  search: string | number
): T => <T>list.find((json: any) => json[key] === search)
