import { IResponseController } from '../../../common/response'

export interface ICoordinates{
    x: number,
    y: number
  }

export interface ISatellite {
    name: string,
    coordinates: ICoordinates
  }

export interface IMessageTopsecret {
    name: string,
    distance: number,
    message: string[]
}

export interface IRequestTopsecret {
    satellites: IMessageTopsecret[]
}

export interface IResponseTopsecret extends IResponseController{
  code: string,
  statusCode: number,
  description: string,
  result?: {
    position: ICoordinates,
    message: string
  }
}

export interface IRequestTopsecretSplit {
  distance: number,
  message: string[]
}

export interface IResponseTopsecretSplit extends IResponseController{
  result?: string
}
