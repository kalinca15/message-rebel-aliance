import tsoa from '@common/tsoa'
import { IDependences } from '@root/infrastructure/libs/dependences'
import mapPointsTrilateration from './actions/mapPointsTrilateration'
import deduceMessage from './actions/deduceMessage'
import findDataSatellites from './actions/findDataSatellites'
import { IRequestTopsecret, IRequestTopsecretSplit, IResponseTopsecret, IResponseTopsecretSplit } from './types'
import { Path } from 'tsoa'
import validSatellite from './actions/validSatellite'
const { Route, SuccessResponse, Body, Inject, Post, Get, Response } = tsoa

@Route('topsecret')
export class MessageController {
  /**
   * It processes a message triangulated by three satellites.
   * Receive the distance from the emitter to each satellite
   * Returns the coordinates of the sender and the encrypted message
   */
  @Post('/')
  @SuccessResponse('200', 'OK')
  @Response(400, 'Invalid request')
  public static async process(@Inject { trilaterationService, responseService }: IDependences, @Body body: IRequestTopsecret): Promise<IResponseTopsecret> {
    const poinst = await mapPointsTrilateration(body.satellites)
    const position = trilaterationService.calculate(poinst)
    const message = deduceMessage(body.satellites)
    return responseService.success({
      position,
      message
    })
  }

  /**
   * Save the message received by a valid satellite
   */
  @Post('/split/{satellite}')
  @SuccessResponse('201', 'Saved satellite message')
  @Response(400, 'Invalid request')
  /**
  * @example satellite "kenobi |  skywalker | sato"
  * */
  public static async save(@Inject { cacheService, responseService }: IDependences, @Body body: IRequestTopsecretSplit, @Path('satellite') { satellite } : any): Promise<IResponseTopsecretSplit> {
    validSatellite(satellite)
    await cacheService.saveObject(satellite, body)
    return responseService.success(satellite + ' satellite information has been saved ')
  }

  /**
   * Returns the coordinates and the coded message according to the data saved from the satellites
   */
  @Get('/split')
  @SuccessResponse('200', 'OK')
  @Response(404, "We don't have enough information to decode the message")
  public static async deduce(@Inject { cacheService, trilaterationService, responseService }: IDependences): Promise<IResponseTopsecret> {
    const messagesTopSecret = await findDataSatellites(cacheService)
    const poinst = await mapPointsTrilateration(messagesTopSecret)
    const position = trilaterationService.calculate(poinst)
    const message = deduceMessage(messagesTopSecret)
    return responseService.success({
      position,
      message
    })
  }
}
