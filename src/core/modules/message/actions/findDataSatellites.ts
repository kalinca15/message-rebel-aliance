import { ICacheRepository } from '@root/common/cache/interface'
import { CodeError, responseCode } from '@root/common/response'
import { satellites } from '@root/core/database/static/satellites'
import { IMessageTopsecret, IRequestTopsecretSplit } from '../types'

export default async (cacheService: ICacheRepository): Promise<IMessageTopsecret[]> => {
  const messagesTopSecret = []
  for (const satellite of satellites) {
    const data: IRequestTopsecretSplit = await cacheService.getObject(satellite.name)
    if (!data) throw new CodeError(responseCode.NOT_FOUND, data, 'we have not received data from the ' + satellite.name + ' satellite ')
    messagesTopSecret.push({
      name: satellite.name, ...data
    })
  }
  return messagesTopSecret
}
