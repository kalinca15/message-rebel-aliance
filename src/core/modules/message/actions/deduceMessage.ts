import { IMessageTopsecret } from '../types'

export default (messages:IMessageTopsecret[]) => {
  const messageMaster = messages[0].message
  const result = []
  for (const i in messageMaster) {
    const word = messageMaster[i]
    const trueWord = word !== '' ? word : messages[1].message[i] !== '' ? messages[1].message[i] : messages[2].message[i]
    result.push(trueWord)
  }
  return result.join(' ')
}
