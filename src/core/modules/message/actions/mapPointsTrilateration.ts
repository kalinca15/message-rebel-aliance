import { IPointTrilateration } from '@root/common/trilateration'
import { findObjInArray } from '@root/core/helpers/utils'
import { IMessageTopsecret } from '../types'
import { satellites } from '@root/core/database/static/satellites'

export default async (messages:IMessageTopsecret[]): Promise<IPointTrilateration[]> => {
  return messages.map((message) => {
    const { x, y } = findObjInArray(satellites, 'name', message.name).coordinates
    return {
      x,
      y,
      distance: message.distance
    }
  })
}
