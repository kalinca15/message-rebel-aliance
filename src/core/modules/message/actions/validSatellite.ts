import { findObjInArray } from '@root/core/helpers/utils'
import { satellites } from '@root/core/database/static/satellites'
import { CodeError, responseCode } from '@root/common/response'

export default (satellite: string) => {
  if (!findObjInArray(satellites, 'name', satellite)) throw new CodeError(responseCode.NOT_FOUND, null, satellite + ' is not a satellite')
  return true
}
