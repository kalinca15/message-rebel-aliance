import { ISatellite } from '@root/core/modules/message/types'

export const satellites : ISatellite[] = [
  {
    name: 'kenobi',
    coordinates: {
      x: -500,
      y: -200
    }
  },
  {
    name: 'skywalker',
    coordinates: {
      x: 100,
      y: -100
    }
  },
  {
    name: 'sato',
    coordinates: {
      x: -500,
      y: 100
    }
  }
]
