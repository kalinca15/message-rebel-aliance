/* eslint-disable */
const {
  Request,
  Body,
  Controller,
  Get,
  Post,
  Put,
  Route,
  SuccessResponse,
  Inject,
  Delete,
  Security,
  Header,
  Query,
  Path,
  Hidden,
  Response
} = require('tsoa');

export default {
  Request,
  Body,
  Controller,
  Get,
  Post,
  Put,
  Route,
  SuccessResponse,
  Inject,
  Delete,
  Security,
  Header,
  Query,
  Path,
  Hidden,
  Response
}
