import {
  createLogger,
  format,
  Logger,
  LoggerOptions,
  transports
} from 'winston'
import { Level } from './enum'
import util from 'util'
import { ILog, ILogRepository } from './interfaces'
import { TransformableInfo } from 'logform'

export class LogService implements ILogRepository {
  private readonly logger: Logger;

  // eslint-disable-next-line no-unused-vars
  constructor(private readonly requestId: string) {
    this.logger = this.create()
  }

  info(logEntity: ILog): void {
    this.logger.log({
      method: logEntity.method,
      level: Level.info,
      message: logEntity.message || '',
      meta: logEntity.data
    })
  }

  error(logEntity: ILog): void {
    this.logger.log({
      method: logEntity.method,
      level: Level.error,
      message: logEntity.message || '',
      meta: logEntity.data
    })
  }

  warn(logEntity: ILog): void {
    this.logger.log({
      method: logEntity.method,
      message: logEntity.message || '',
      level: Level.warn,
      meta: logEntity.data
    })
  }

  debug(logEntity: ILog): void {
    this.logger.log({
      method: logEntity.method,
      message: logEntity.message || '',
      level: Level.debug,
      meta: logEntity.data
    })
  }

  getCreateOptions(): LoggerOptions {
    return {
      level: Level.silly,
      format: format.combine(
        format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf((info) => this.transformData(info))
      ),
      transports: [new transports.Console()]
    }
  }

  create(): Logger {
    const config: LoggerOptions = this.getCreateOptions()

    return createLogger(config)
  }

  transformData(info: TransformableInfo): string {
    const parsedMeta = info.meta ? this.parseMeta(info.meta) : ''
    const meta = parsedMeta ? `- Meta: ${parsedMeta}` : ''
    return `APP RequestId: ${this.requestId} ${
      info.method ? `- Method: ${info.method}` : ''
    } [${info.level}] ${info.message ? info.message : ''}${meta} }`
  }

  private parseMeta(meta:unknown) {
    if (typeof meta === 'object') {
      return JSON.stringify(meta, (_name, value) => {
        if (value instanceof Error) {
          return util.format(value)
        }
        return value
      })
    }
    return meta
  }
}
