
import Redis from 'ioredis'
import { ILogRepository } from '../logger/interfaces'
import { ICacheRepository, ICacheRepositoryStatic } from './interface'

@staticImplements<ICacheRepositoryStatic>()
export class CacheRedis implements ICacheRepository {
  private client;
  private expired;
  private static instance: CacheRedis;
  private loggerService: ILogRepository;

  constructor(host:string, expired:number, loggerService: ILogRepository) {
    this.loggerService = loggerService
    this.loggerService.debug({
      method: 'CacheRedis.constructor',
      data: { host, expired }
    })
    const clientNoAsync = new Redis(host)
    this.client = clientNoAsync
    this.expired = expired
  }

  async saveObject(
    key: string,
    value: any,
    expired: number = this.expired
  ): Promise<any> {
    const valueStringObj = JSON.stringify(value, null, 0)
    return await this.client.set(key, valueStringObj, 'EX', expired)
  }

  async getObject(key: string): Promise<any> {
    const data = await this.client.get(key)
    let objectData
    if (data) {
      objectData = JSON.parse(data)
    }
    return objectData
  }

  async getPropertyObject(key: string, property: string): Promise<any> {
    const context = await this.getObject(key)
    return context[property] || undefined
  }

  async del(key: string): Promise<any> {
    return await this.client.del(key)
  }

  async get(key: string): Promise<any> {
    const result = await this.client.get(key)
    this.loggerService.debug({
      method: 'CacheService.get',
      data: { key, result }
    })
    return result
  }

  async set(
    key: string,
    value: any,
    expired: number = this.expired
  ): Promise<any> {
    const result = await this.client.set(key, value, 'EX', expired)
    this.loggerService.debug({
      method: 'CacheService.set',
      data: { key, result }
    })
    return result
  }

  public static getInstance(host:string, expired:number, loggerService: ILogRepository) {
    if (!CacheRedis.instance) {
      CacheRedis.instance = new CacheRedis(host, expired, loggerService)
    }
    return CacheRedis.instance
  }
}

function staticImplements<T>() {
  return <U extends T>(constructor: U) => {
    // eslint-disable-next-line no-unused-expressions
    constructor
  }
}
