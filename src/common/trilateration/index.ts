import { ICoordinates } from '@root/core/modules/message/types'
const trilateration = require('node-trilateration')

export interface IPointTrilateration {
    distance: number,
    x: number,
    y:number
  }

export interface ITrilaterationRepository {
    calculate(points:IPointTrilateration[]): ICoordinates
  }
export class TrilaterationRepository implements ITrilaterationRepository {
  calculate(points:IPointTrilateration[]) {
    return trilateration.calculate(points)
  }
}
