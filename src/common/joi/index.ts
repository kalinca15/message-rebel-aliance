import { CodeError, responseCode } from '@root/common/response'
import * as Joi from 'joi'

export default async (schema: Joi.ObjectSchema<any>, data: unknown) => {
  try {
    await schema.validateAsync(data, {
      abortEarly: false,
      allowUnknown: false
    })
  } catch (error) {
    const details = error.details || null
    throw new CodeError(responseCode.INVALID_BODY, details)
  }
}
