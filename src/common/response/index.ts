// istanbul ignore file
// WHY?: Nothing to test.

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { ILogRepository } from '@root/common/logger/interfaces'

export enum responseCode {
    UNCAUGHT_EXCEPTION = 'UNCAUGHT_EXCEPTION',
    INVALID_BODY = 'INVALID_BODY',
    INVALID_QUERY = 'INVALID_QUERY',
    DUPLICATE_ENTRY = 'DUPLICATE_ENTRY',
    NOT_FOUND = 'NOT_FOUND',
    INVALID_PASSWORD = 'INVALID_PASSWORD',
    EXPIRED_TOKEN = 'EXPIRED_TOKEN',
    TOKEN_INVALID = 'TOKEN_INVALID',
    NOT_ALLOWED = 'NOT_ALLOWED',
    UNKNOWN = 'UNKNOWN',
    OK = 'SUCCESS',
    NO_CONTENT = 'NO_CONTENT',
    CREATED = 'CREATED'
}

const defaultResponses = [
  { code: responseCode.UNCAUGHT_EXCEPTION, statusCode: 500, description: 'Internal Server error', result: null },
  { code: responseCode.INVALID_BODY, statusCode: 400, description: 'Invalid request', result: null },
  { code: responseCode.INVALID_QUERY, statusCode: 400, description: 'Invalid query', result: null },
  { code: responseCode.DUPLICATE_ENTRY, statusCode: 400, description: '', result: null },
  { code: responseCode.NOT_FOUND, statusCode: 404, description: '', result: null },
  { code: responseCode.NOT_ALLOWED, statusCode: 403, description: '', result: null },
  { code: responseCode.OK, statusCode: 200, description: 'The request has been successful', result: {} },
  { code: responseCode.CREATED, statusCode: 201, description: 'Resource successfully created', result: {} }
]

export interface IResponseController {
  code: string,
  statusCode: number,
  description: string,
  result?: any
}

export class CodeError extends Error {
  data: unknown;
  constructor(public errorCode: responseCode, data?: unknown, description?: string) {
    super(description)
    this.data = data || null
  }
}

export interface IResponseRepository {
  success<T>(data?: T, code?: string): IResponseController,
  noContent<T>(data?: T): IResponseController,
  invalid<T>(data?: T, description?: string): IResponseController,
  error(error: CodeError): IResponseController
}

export class ResponseHandler implements IResponseRepository {
  private logger: ILogRepository;
  private method: string;
  constructor(_logger: ILogRepository, method: string) {
    this.logger = _logger
    this.method = method
    this.logger.debug({ method })
  }

  private set<T>(code: string, data?: T): IResponseController {
    const newResponse = defaultResponses.filter(e => e.code === code)[0]
    newResponse.result = data || null
    return newResponse
  }

  success<T>(data?: T, code = responseCode.OK): IResponseController {
    this.logger.debug({
      message: 'Response.success',
      method: this.method,
      data
    })
    return this.set(code, data)
  }

  noContent<T>(data?: T): IResponseController {
    this.logger.info({
      method: this.method,
      message: 'No content response',
      data: data || null
    })
    return this.set(responseCode.NO_CONTENT, data)
  }

  invalid<T>(data?: T, description?: string): IResponseController {
    const response = this.set(responseCode.INVALID_BODY, data)
    if (description) response.description = description
    this.logger.info({
      method: this.method,
      data: data || null,
      message: description || null
    })
    return response
  }

  error(error: CodeError): IResponseController {
    const response = this.set(error.errorCode || responseCode.UNCAUGHT_EXCEPTION, error.data)
    response.description = error.message || response.description
    if (response.statusCode > 500) {
      this.logger.error({
        method: this.method,
        message: response.description,
        data: error
      })
    }
    return response
  }
}
