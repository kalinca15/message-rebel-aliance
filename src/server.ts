import http from 'http'
import app from '@infrastructure/app'
import environment from '@environment'

const env = process.env.NODE_ENV || 'local'

export const server = http.createServer(app)
server.listen(+environment.PORT, () => {
  console.log(`Server running at port ${environment.PORT} in ${env}`)
})
