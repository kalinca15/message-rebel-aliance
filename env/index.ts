import dotenv from 'dotenv'
const env = process.env.NODE_ENV || 'local'
// eslint-disable-next-line no-unused-expressions
dotenv.config({ path: `./config/${env}` }).parsed

export default {
  PORT: process.env.PORT || 3000,
  REDIS_URL: process.env.REDIS_URL as string,
  REDIS_EXPIRED: 3600,
  REDIS_KEY_SATELITE: 'SATELITES'
}
