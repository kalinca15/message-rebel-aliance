module.exports = {
  env: {
    es2020: true,
    node: true,
    jest: true
  },
  extends: [
    'standard'
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
    project: ['tsconfig.json']
  },
  ignorePatterns: ['.eslintrc.js', 'webpack.config.js'],
  rules: {
    'space-before-function-paren': 'off',
    '@typescript-eslint/no-redeclare': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-unsafe-return': 'off',
    indent: ['warn', 2],
    'no-irregular-whitespace': 'warn',
    '@typescript-eslint/naming-convention': [
      'warn',
      {
        selector: 'class',
        format: ['PascalCase']
      },
    ]
  }
}
